class CreateDocClasses < ActiveRecord::Migration
  def change
    create_table :doc_classes do |t|
      t.string :name
      t.text :description
      t.references :project
      t.timestamps
    end

    add_index :doc_classes, :project_id
  end

end
