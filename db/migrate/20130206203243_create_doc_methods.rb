class CreateDocMethods < ActiveRecord::Migration
  def change
    create_table :doc_methods do |t|
      t.string :name
      t.text :description
      t.references :doc_class

      t.timestamps
    end
  end
end
