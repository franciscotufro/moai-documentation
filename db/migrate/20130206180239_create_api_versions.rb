class CreateApiVersions < ActiveRecord::Migration
  def change
    create_table :api_versions do |t|
      t.string :version_name
      t.string :slug
      t.references :project
      t.timestamps
    end
    add_index :api_versions, :slug
    add_index :api_versions, :project_id
  end
end
