class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.timestamps
    end

    add_index :projects, :slug
    add_index :projects, :name
  end
end
