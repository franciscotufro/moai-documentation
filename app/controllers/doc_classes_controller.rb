class DocClassesController < ApplicationController

  def show
    @project = Project.find_by_slug(params[:project_id])
    @version = ApiVersion.find_by_slug(params[:api_version_id])
    @doc_class = DocClass.find_by_name(params[:id])
  end

end
