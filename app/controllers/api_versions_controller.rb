class ApiVersionsController < ApplicationController

  def show
    @project = Project.find_by_slug(params[:project_id])
    @version = ApiVersion.find_by_slug(params[:id])
  end
end
