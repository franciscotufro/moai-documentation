class Project < ActiveRecord::Base
  attr_accessible :name, :slug, :description

  #validates :name, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true

  has_many :api_versions
  has_many :doc_classes
  
  def to_param
    slug
  end

  def to_s
    name
  end
end
