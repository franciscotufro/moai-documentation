class ApiVersion < ActiveRecord::Base
  attr_accessible :version_name, :project_id, :slug

  #validates :version_name, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true

  belongs_to :project
  has_many :doc_classes

  def to_param
    slug
  end

  def to_s
    version_name
  end

end
