class DocMethod < ActiveRecord::Base
  attr_accessible :doc_class_id, :description, :name
  belongs_to :doc_class

  def to_param
    name
  end

  def to_s
    name
  end
end
