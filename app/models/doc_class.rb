class DocClass < ActiveRecord::Base
  attr_accessible :description, :name, :project_id
  belongs_to :project

  has_many :doc_methods

  default_scope order('name ASC')
  def to_s
    name
  end

  def to_param
    name
  end
end
